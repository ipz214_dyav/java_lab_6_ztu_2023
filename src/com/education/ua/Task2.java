package com.education.ua;


public class Task2 {

    public static void main(String[] args) {
        String login = "gsdfsdfsdfsdfsdfsdfsdfsdfsdf";
        String password = "YourPassword123_";
        String confirmPassword = "YourPassword123_";

        try {
            boolean result = checkCredentials(login, password, confirmPassword);
            System.out.println("³���������� ����� �� ������: " + result);
        } catch (WrongLoginException | WrongPasswordException e) {
            System.out.println("�������: " + e.getMessage());
        }
    }

    public static boolean checkCredentials(String login,String password, String confirmPassword) throws WrongLoginException, WrongPasswordException {
        if (login.length() > 20 || !login.matches("^[a-zA-Z0-9_]*$")) {
            throw new WrongLoginException();
        }

        if (password.length() > 20 || !password.matches("^[a-zA-Z0-9_]*$") || !password.equals(confirmPassword)) {
            throw new WrongPasswordException();
        }

        return true;
    }
}
