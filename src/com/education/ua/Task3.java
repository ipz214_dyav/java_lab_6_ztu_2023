package com.education.ua;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Formatter;
import java.util.Locale;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) throws IOException {
        Check check = new Check();
        Locale englishLocale = new Locale("en", "US");
        Formatter check2 = check.printCheck(englishLocale);

        FileWriter fileWriter = new FileWriter("./directory_for_files/check.txt");
        fileWriter.write(String.valueOf(check2));
        fileWriter.close();

        FileReader fileReader = new FileReader("./directory_for_files/check.txt");
        Scanner scan = new Scanner(fileReader);
        while (scan.hasNextLine()){
            System.out.println( scan.nextLine());
        }
        fileReader.close();
    }
}
