package com.education.ua;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Task7 {
    public static void main(String[] args) throws IOException {
        FileOutputStream zipFile = new FileOutputStream("./archive.zip");
        ZipOutputStream zip = new ZipOutputStream(zipFile);

        File directoryForFiles = new File("./directory_for_files");
        File[] contents = directoryForFiles.listFiles();
        if (contents != null) {
            for (File item : contents) {
                ZipEntry entry = new ZipEntry(item.getName());
                zip.putNextEntry(entry);

                FileInputStream fileInputStream = new FileInputStream(item);
                byte[] buffer = new byte[1024];
                int length;
                while ((length = fileInputStream.read(buffer)) > 0) {
                    zip.write(buffer, 0, length);
                }

                zip.closeEntry();
                fileInputStream.close();
            }
        }

        zip.close();
        zipFile.close();


        FileInputStream zipFileInput = new FileInputStream("./archive.zip");
        ZipInputStream zipStream = new ZipInputStream(zipFileInput);

        ZipEntry zipEntry;
        while ((zipEntry = zipStream.getNextEntry()) != null) {
            System.out.println(zipEntry.getName());
        }

        zipStream.close();
        zipFileInput.close();
    }
}
